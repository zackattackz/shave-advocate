(ns shave-advocate.handler.example
  (:require [ataraxy.core :as ataraxy]
            [ataraxy.response :as response] 
            [clojure.java.io :as io]
            [integrant.core :as ig]))

(defmethod ig/init-key :shave-advocate.handler/example [_ options]
  (fn [{[_] :ataraxy/result}]
    [::response/ok (io/resource "shave_advocate/handler/example/example.html")]))
